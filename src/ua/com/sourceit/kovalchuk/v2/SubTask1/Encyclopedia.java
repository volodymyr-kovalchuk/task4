package ua.com.sourceit.kovalchuk.v2.SubTask1;

public class Encyclopedia extends Book {
    public Encyclopedia(String codeISBN, String author, String name, int year, String publishingHouse) {
        super(codeISBN, author, name, year, publishingHouse);
    }

    @Override
    protected String printTypeOfBook() {
        return "This book is Encyclopedia!";
    }
}
