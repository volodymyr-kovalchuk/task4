package ua.com.sourceit.kovalchuk.v2.SubTask1;

public abstract class Book {
    String codeISBN;
    String author;
    String name;
    int year;
    String publishingHouse;

    public Book(String codeISBN, String author, String name, int year, String publishingHouse) {
        this.codeISBN = codeISBN;
        this.author = author;
        this.name = name;
        this.year = year;
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return printTypeOfBook() + '\n' +
                "codeISBN='" + codeISBN + '\'' + '\n' +
                "author='" + author + '\'' + '\n' +
                "name='" + name + '\'' + '\n' +
                "year=" + year + '\n' +
                "publishingHouse='" + publishingHouse + '\'' + '\n';
    }

    protected abstract String printTypeOfBook();
}
