package ua.com.sourceit.kovalchuk.v2.SubTask1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        books.add(new Encyclopedia("9781912920471",
                "Christopher Lloyd",
                "Britannica All New Children's Encyclopedia",
                2020,
                "Britannica Books"
        ));
        books.add(new Handbook("9780241438114",
                "Frida Ramstedt",
                "The Interior Design Handbook",
                2020,
                "Particular Books"
        ));

        books.forEach(System.out::println);
    }
}
