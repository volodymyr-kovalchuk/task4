package ua.com.sourceit.kovalchuk.v2;

import java.io.*;
import java.util.*;

public class SubTask2 {
    public static final String INPUT_FILE = "src/ua/com/sourceit/kovalchuk/v2/SubTask2.java";
    public static final String OUTPUT_CLASS_NAME = "OutputClassFromSubTask2";
    public static final String OUTPUT_FILE = "src/ua/com/sourceit/kovalchuk/v2/" + OUTPUT_CLASS_NAME + ".java";

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();
        readFromFileAndReplaceWordsPublic(stringList);
        writeToOutputFile(stringList);
    }

    private static void readFromFileAndReplaceWordsPublic(List<String> stringList) {
        try (BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE))) {
            while (true) {
                String currentLine = br.readLine();
                if (currentLine == null) break;
                String modifiedLine = replaceWordsPublic(currentLine);
                stringList.add(modifiedLine);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeToOutputFile(List<String> stringList) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(OUTPUT_FILE))) {
            for (String currentLine : stringList) {
                bw.write(currentLine);
                bw.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String replaceWordsPublic(String currentLine) {
        if (currentLine.contains("class"))
            return currentLine.replace(getInputClassName(), OUTPUT_CLASS_NAME);
        else {
            if (currentLine.contains("public")
                    && !currentLine.contains("static void main")
            )
                return currentLine.replaceAll("public", "private");
            else return currentLine;
        }
    }

    private static String getInputClassName() {
        int startIndex = INPUT_FILE.lastIndexOf('/') + 1;
        int endIndex = INPUT_FILE.lastIndexOf('.');
        return INPUT_FILE.substring(startIndex, endIndex);
    }
}
