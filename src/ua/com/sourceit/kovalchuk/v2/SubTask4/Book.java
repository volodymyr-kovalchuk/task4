package ua.com.sourceit.kovalchuk.v2.SubTask4;

public class Book {
    public int bookNumber;
    public boolean isAvailableForGivingInHand;
    public boolean isGiven;
    public int numberOfUser;
    public long startUsingTime;
    public long bookUsingTime;

    public Book(int bookNumber, boolean isGivenInHand) {
        this.bookNumber = bookNumber;
        this.isAvailableForGivingInHand = isGivenInHand;
        this.isGiven = false;
    }

    public void giveBook(int numberOfUser, long startUsingTime, long bookUsingTime) {
        this.isGiven = true;
        this.numberOfUser = numberOfUser;
        this.startUsingTime = startUsingTime;
        this.bookUsingTime = bookUsingTime;
    }

    public void returnBook() {
        this.isGiven = false;
        this.numberOfUser = 0;
        this.startUsingTime = 0;
        this.bookUsingTime = 0;
    }
}