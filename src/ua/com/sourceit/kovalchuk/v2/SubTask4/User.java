package ua.com.sourceit.kovalchuk.v2.SubTask4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class User {
    public int numberOfUser;
    public List<Book> booksInUse = new ArrayList<>();

    public User(int numberOfUser) {
        this.numberOfUser = numberOfUser;
    }

    public void addBook(Book book) {
        booksInUse.add(book);
    }

    public List<Book> checkTimeAndReturnBook(Long currentTime) {
        List<Book> booksForReturning = booksInUse.stream()
                .filter(book -> currentTime >= book.startUsingTime + book.bookUsingTime)
                .collect(Collectors.toList());
        booksForReturning.forEach(book -> booksInUse.remove(book));
        return booksForReturning;
    }

    protected static User findUserByNumber(int numberOfUser) {
        for (User user : Main.users) {
            if (user.numberOfUser == numberOfUser) return user;
        }
        return new User(numberOfUser);
    }
}
