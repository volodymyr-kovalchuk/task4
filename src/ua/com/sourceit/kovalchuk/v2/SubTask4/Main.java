package ua.com.sourceit.kovalchuk.v2.SubTask4;

import java.util.*;
import java.util.concurrent.*;

public class Main {
    private static final int MAX_USERS_NUMBER = 3;
    private static final int NUMBER_OF_BOOKS_IN_LIBRARY = 10;
    private static final int MAX_BOOK_USING_TIME_SEC = 2;
    private static final int MAX_TIME_BETWEEN_GIVING_BOOK_SEC = 1;
    private static final int LIBRARY_TIME_EXECUTION_SEC = 10;
    private static final CopyOnWriteArrayList<Book> booksInLibrary = new CopyOnWriteArrayList<>();
    protected static final List<User> users = new ArrayList<>();
    private static final Random rnd = new Random();

    public static void main(String[] args) {
        int numberOfUsers = initializeUsersAndLibrary();

        ExecutorService taskExecutor = Executors.newFixedThreadPool(numberOfUsers);
        long startExecutionTime = System.currentTimeMillis();
        long nextUserTime = 0;

        while (!isCallCentreExecutionTimeOver(startExecutionTime)) {
            if (isItTimeForNextActionWithBook(nextUserTime)) {
                tryToTakeNewBook(taskExecutor);
                nextUserTime = System.currentTimeMillis() +
                        rnd.nextInt(MAX_TIME_BETWEEN_GIVING_BOOK_SEC * 1000) + 1;
            }
        }

        taskExecutor.shutdown();
    }

    private static int initializeUsersAndLibrary() {
        int numberOfUsers = rnd.nextInt(MAX_USERS_NUMBER) + 1;
        fillUsers(numberOfUsers);
        fillLibraryBooks();
        System.out.println("Library with " + NUMBER_OF_BOOKS_IN_LIBRARY + " books start to work! " +
                numberOfUsers + " user(s) use it!\n");
        return numberOfUsers;
    }

    private static void fillLibraryBooks() {
        for (int i = 0; i < NUMBER_OF_BOOKS_IN_LIBRARY; i++) {
            boolean isGivenInHand = rnd.nextInt(2) != 0;
            booksInLibrary.add(new Book(i, isGivenInHand));
        }
    }

    private static void fillUsers(int numberOfUsers) {
        for (int i = 0; i < numberOfUsers; i++) {
            users.add(new User(i + 1));
        }
    }

    private static boolean isCallCentreExecutionTimeOver(long startExecutionTime) {
        return System.currentTimeMillis() > startExecutionTime + LIBRARY_TIME_EXECUTION_SEC * 1000;
    }

    private static boolean isItTimeForNextActionWithBook(long nextUserTime) {
        return System.currentTimeMillis() >= nextUserTime;
    }

    private static void tryToTakeNewBook(ExecutorService taskExecutor) {
        UseBookTask currentSpeakTask = new UseBookTask();
        taskExecutor.submit(currentSpeakTask);
    }

    private static class UseBookTask implements Runnable {
        private static final int USER_NUM_BEGIN_INDEX = 14;

        @Override
        public void run() {
            User user = initializeSearchingBookUser();
            Book neededBook = initializeNeededBook();
            checkIfUserDontTakeBookAndGiveItToUser(user, neededBook);
            checkBooksForReturning(user);
        }

        private static User initializeSearchingBookUser() {
            int userNumber = Integer.parseInt(Thread.currentThread().getName().substring(USER_NUM_BEGIN_INDEX));
            return User.findUserByNumber(userNumber);
        }

        private static Book initializeNeededBook() {
            int neededBookNumber = rnd.nextInt(NUMBER_OF_BOOKS_IN_LIBRARY) + 1;
            return booksInLibrary.get(neededBookNumber);
        }

        private void checkIfUserDontTakeBookAndGiveItToUser(User user, Book neededBook) {
            if (neededBook.numberOfUser == user.numberOfUser) {
                System.out.println("The requested book №" + neededBook.bookNumber +
                        " is already in the user №" + user.numberOfUser + " possession.");
            } else checkIfBookInLibraryAndGiveItToUser(user, neededBook);
        }

        private void checkIfBookInLibraryAndGiveItToUser(User user, Book neededBook) {
            if (neededBook.isGiven) {
                System.out.println("User №" + user.numberOfUser + " want to take book №" + neededBook.bookNumber +
                        ", but the requested book is already given to user №" + neededBook.numberOfUser + ".");
            } else {
                checkAvailabilityForGivingOnHandsAndGiveItToUser(user, neededBook);
            }
        }

        private void checkAvailabilityForGivingOnHandsAndGiveItToUser(User user, Book neededBook) {
            boolean needToGiveInHand = rnd.nextInt(2) != 0;
            if (needToGiveInHand && !neededBook.isAvailableForGivingInHand) {
                System.out.println("User №" + user.numberOfUser + " want to take book №" +
                        neededBook.bookNumber + " in hand, but this book isn't available for giving in hands!");
            } else {
                giveBookToUser(user, neededBook, needToGiveInHand);
            }
        }

        private void giveBookToUser(User user, Book neededBook, boolean needToGiveInHand) {
            int bookUsingTime = rnd.nextInt(MAX_BOOK_USING_TIME_SEC * 1000) + 1;
            takeBook(neededBook, user.numberOfUser, needToGiveInHand, bookUsingTime);
            user.addBook(neededBook);
        }

        private void takeBook(Book book, int userNumber, boolean needToGiveInHand, int bookUsingTime) {
            book.giveBook(userNumber, System.currentTimeMillis(), bookUsingTime);
            System.out.println("User №" + userNumber + " take a book №" + book.bookNumber + " for using in " +
                    (needToGiveInHand ? "hands" : "library") + "!");
        }

        private static void checkBooksForReturning(User user) {
            List<Book> booksForReturning = user.checkTimeAndReturnBook(System.currentTimeMillis());
            if (booksForReturning.size() > 0) System.out.println("User №" + user.numberOfUser + " return:");

            for (Book book : booksForReturning) {
                long bookUsingTime = book.bookUsingTime;
                System.out.println("      book №" + book.bookNumber + " after using for " +
                        bookUsingTime / 1000d + " seconds!");
                book.returnBook();
            }
        }
    }
}