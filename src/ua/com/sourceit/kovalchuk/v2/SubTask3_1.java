package ua.com.sourceit.kovalchuk.v2;

import java.util.*;

public class SubTask3_1 {
    public static final int BEGINNING_NUMBER_OF_PEOPLE = 100_000;

    public static void main(String[] args) {
        List<Integer> arrayList = fillList(new ArrayList<>());
        List<Integer> linkedList = fillList(new LinkedList<>());

        long t1 = System.currentTimeMillis();
        deleteEachSecondPerson(arrayList);
        long t2 = System.currentTimeMillis();
        deleteEachSecondPerson(linkedList);
        long t3 = System.currentTimeMillis();

        System.out.println("Execution time with ArrayList = " + (t2 - t1) / 1000d + " seconds!");
        System.out.println("Execution time with LinkedList = " + (t3 - t2) / 1000d + " seconds!");
    }

    private static List<Integer> fillList(List<Integer> numberOfPeople) {
        for (int i = 1; i <= BEGINNING_NUMBER_OF_PEOPLE; i++) numberOfPeople.add(i);
        return numberOfPeople;
    }

    private static void deleteEachSecondPerson(List<Integer> numberOfPeople) {
        boolean deleteEvenNumbers = true;
        while (numberOfPeople.size() != 1) {
            if (deleteEvenNumbers) deleteEvenNumbers = deleteEachEvenNumber(numberOfPeople);
            else deleteEvenNumbers = deleteEachOddNumber(numberOfPeople);
        }
        System.out.println("The last person is №" + numberOfPeople.get(0));
    }

    private static boolean deleteEachEvenNumber(List<Integer> numberOfPeople) {
        int amountOfPeople = numberOfPeople.size();
        boolean deleteNextEvenNumbers = amountOfPeople % 2 == 0;
        int amountOfDeletingPeople = amountOfPeople / 2;

        for (int i = 1; i <= amountOfDeletingPeople; i++) {
                int removedNumber = numberOfPeople.remove(i);
                System.out.println("Deleted №" + removedNumber);
        }
        return deleteNextEvenNumbers;
    }

    private static boolean deleteEachOddNumber(List<Integer> numberOfPeople) {
        int amountOfPeople = numberOfPeople.size();
        boolean deleteNextEvenNumbers = amountOfPeople % 2 != 0;

        int amountOfDeletingPeople;
        if (deleteNextEvenNumbers) amountOfDeletingPeople = amountOfPeople / 2 + 1;
        else amountOfDeletingPeople = amountOfPeople / 2;

        for (int i = 0; i < amountOfDeletingPeople; i++) {
            int removedNumber = numberOfPeople.remove(i);
            System.out.println("Deleted №" + removedNumber);
        }
        return deleteNextEvenNumbers;
    }
}
