package ua.com.sourceit.kovalchuk.v2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class SubTask3_2 {
    public static final int NUMBER_OF_ROWS = 4;
    public static final int NUMBER_OF_PARKING_SPACES_IN_ROW = 250;
    private static final Random rnd = new Random();

    public static void main(String[] args) {
        List<HashMap<Integer, Boolean>> parking = initializeParking();

        Integer parkingSpace = driveIntoParkingLot(parking, 2);
        System.out.println();
        leaveParkingLot(parking, 2, parkingSpace);

        System.out.println();
        parkingSpace = driveIntoParkingLot(parking, 4);
        System.out.println();
        leaveParkingLot(parking, 4, parkingSpace);
    }

    private static Integer driveIntoParkingLot(List<HashMap<Integer, Boolean>> parking, int row) {
        System.out.println("Enter into the parking!");
        System.out.println("Try parking into the " + row + " row!");
        driveToSelectedRow(row);
        System.out.println("Turning the car into the " + row + " row!");
        return driveToTheFreeParkingSpace(parking, row);
    }

    private static Integer driveToTheFreeParkingSpace(List<HashMap<Integer, Boolean>> parking, int row) {
        HashMap<Integer, Boolean> currentRow = parking.get(row - 1);
        for (int i = 1; i <= NUMBER_OF_PARKING_SPACES_IN_ROW; i++) {
            System.out.print("Drive through the " + i + " parking space. ");
            if (currentRow.get(i)) {
                System.out.println("It's free now! Parking in " + row + " row at " + i + " parking space!");
                currentRow.put(i, false);
                return i;
            } else {
                if (i != NUMBER_OF_PARKING_SPACES_IN_ROW) System.out.println("It's busy now, let's go further!");
                else System.out.println("The whole row is busy, let's go back!");
            }
        }
        return null;
    }

    private static void driveToSelectedRow(int row) {
        for (int i = 1; i <= row; i++) {
            System.out.println("Drive through the " + i + " row!");
        }
    }

    private static void leaveParkingLot(List<HashMap<Integer, Boolean>> parking, int row, Integer parkingSpace) {
        if (checkIsCorrectData(row, parkingSpace)) return;

        System.out.println("Leave parking from the " + parkingSpace + " parking space at " + row + " row!");
        System.out.println("Turning the car from the " + parkingSpace + " parking space at " + row + " row!");
        leaveFromCurrentParkingSpace(parking, row, parkingSpace);
        System.out.println("Turning the car from the " + row + " row to exit!");
        leaveFromCurrentRow(row);
        System.out.println("Leave parking!");
    }

    private static void leaveFromCurrentRow(int row) {
        for (int i = row; i >= 1; i--) {
            System.out.println("Drive through the " + i + " row!");
        }
    }

    private static void leaveFromCurrentParkingSpace(List<HashMap<Integer, Boolean>> parking, int row, int parkingSpace) {
        HashMap<Integer, Boolean> currentRow = parking.get(row - 1);
        currentRow.put(parkingSpace, true);
        for (int i = parkingSpace; i >= 1; i--) {
            System.out.println("Drive through the " + i + " parking space to exit.");
        }
    }

    private static boolean checkIsCorrectData(int row, Integer parkingSpace) {
        if (row < 1 || row > NUMBER_OF_ROWS ||
                parkingSpace == null || parkingSpace < 1 || parkingSpace > NUMBER_OF_PARKING_SPACES_IN_ROW) {
            System.out.println("Entered incorrect number of row or parking space for leaving parking!");
            return true;
        }
        return false;
    }

    private static List<HashMap<Integer, Boolean>> initializeParking() {
        List<HashMap<Integer, Boolean>> parking = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ROWS; i++) {
            parking.add(initializeRow());
        }
        return parking;
    }

    private static HashMap<Integer, Boolean> initializeRow() {
        HashMap<Integer, Boolean> row = new HashMap<>();
        for (int i = 1; i <= NUMBER_OF_PARKING_SPACES_IN_ROW; i++) {
            boolean isEmpty = rnd.nextInt(NUMBER_OF_PARKING_SPACES_IN_ROW) == 0;
            row.put(i, isEmpty);
        }
        return row;
    }
}
