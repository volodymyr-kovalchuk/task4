package ua.com.sourceit.kovalchuk.v1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Notepad {
    Map<String, List<Note>> notes;

    public static void main(String[] args) {
        Notepad notepad = new Notepad();
        Scanner sc = new Scanner(System.in);
        boolean exit = true;
        while (exit) {
            System.out.println("Press 1 to add new note\n" +
                    "Press 2 to show all notes\n" +
                    "Press 3 to show notes by date\n" +
                    "Press 4 to exit");
            String pressedKey = sc.nextLine();
            switch (pressedKey) {
                case ("1"):
                    notepad.addNote(sc);
                    break;
                case ("2"):
                    showAllNotes(notepad);
                    break;
                case ("3"):
                    showNotesByDate(sc, notepad);
                    break;
                case ("4"):
                    exit = false;
                    break;
                default:
                    System.out.println("Pressed incorrect key, please try again!\n");
            }
        }
    }

    private static void showNotesByDate(Scanner sc, Notepad notepad) {
        System.out.println("Please enter searching date in \"yyyy-MM-dd\" format:");
        String date = sc.nextLine();
        try {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            ft.parse(date);

            if (notepad.notes.containsKey(date)) {
                System.out.println(date);
                System.out.println(notepad.notes.get(date) + "\n");
            } else System.out.println("There is no notes for the date " + date + "\n");
        } catch (ParseException e) {
            System.out.println("Incorrect date format: " + date + "\n");
        }
    }

    private static void showAllNotes(Notepad notepad) {
        for (Map.Entry<String, List<Note>> entry : notepad.notes.entrySet()) {
            System.out.println(entry.getKey());
            List<Note> noteList = entry.getValue();
            for (Note note : noteList) {
                System.out.println(note + "\n");
            }
        }
    }

    Notepad() {
        notes = new HashMap<>();
    }

    void addNote(Scanner sc) {
        System.out.println("Please enter note's summary:");
        String summary = sc.nextLine();
        System.out.println("Please enter note's text:");
        String noteText = sc.nextLine();

        new Note(summary, noteText);
        System.out.println("New note added!\n");
    }

    class Note {
        String summary;
        String noteText;

        Note(String summary, String noteText) {
            this.summary = summary;
            this.noteText = noteText;

            addToNotesListByDate();
        }

        private void addToNotesListByDate() {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateString = ft.format(new Date());
            if (notes.containsKey(currentDateString)) {
                notes.get(currentDateString).add(this);
            } else {
                List<Note> noteList = new ArrayList<>();
                noteList.add(this);
                notes.put(currentDateString, noteList);
            }
        }

        public String toString() {
            return ("Summary: " + summary + "\n" + "Note: " + noteText);
        }
    }
}