package ua.com.sourceit.kovalchuk.v1;

import java.io.*;
import java.util.*;

public class SubTask3 {

    public static final String INPUT_FILE = "src/ua/com/sourceit/kovalchuk/v1/in3.txt";
    public static final String OUTPUT_FILE = "src/ua/com/sourceit/kovalchuk/v1/out3.txt";

    public static void main(String[] args) {
        Stack<String> stringList = new Stack<>();
        readFromFile(stringList);
        writeToFile(stringList);
    }

    private static void writeToFile(Stack<String> stringList) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(OUTPUT_FILE))) {
            while (!stringList.isEmpty()) {
                String currentLine = stringList.pop();
                bw.write(currentLine);
                bw.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void readFromFile(Stack<String> stringList) {
        try (BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE))) {
            while (true) {
                String currentLine = br.readLine();
                if (currentLine == null) break;
                stringList.add(currentLine);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
