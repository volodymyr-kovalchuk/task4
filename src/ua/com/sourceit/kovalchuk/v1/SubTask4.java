package ua.com.sourceit.kovalchuk.v1;

import java.util.Stack;

public class SubTask4 {
    public static final long ENTERED_NUMBER = 1234567;

    public static void main(String[] args) {
        Stack<Character> stack = new Stack<>();
        String enteredNumber = String.valueOf(ENTERED_NUMBER);
        for (int i = 0; i < enteredNumber.length(); i++) {
            stack.add(enteredNumber.charAt(i));
        }
        while (!stack.isEmpty()) {
            System.out.print(stack.pop());
        }
    }
}
