package ua.com.sourceit.kovalchuk.v1;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class SubTask5 {
    private static final int MAX_OPERATORS_NUMBER = 3;
    private static final int MAX_TIME_BETWEEN_CALLS_SEC = 1;
    private static final int MAX_CALL_SPEAKING_TIME_SEC = 7;
    private static final int MAX_WAITING_FOR_CALL_TIME_SEC = 4;
    private static final int CALL_CENTRE_TIME_EXECUTION_SEC = 10;
    static ArrayBlockingQueue<Map.Entry<Future<?>, SpeakTask>> queue = new ArrayBlockingQueue<>(10);
    private static final AtomicInteger CLIENT_NUMBER = new AtomicInteger(0);
    private static final Random rnd = new Random();

    public static void main(String[] args) {
        int numberOfOperators = getNumberOfOperators();
        ExecutorService taskExecutor = Executors.newFixedThreadPool(numberOfOperators);
        long startExecutionTime = System.currentTimeMillis();
        long nextCallTime = 0;

        while (!isCallCentreExecutionTimeOver(startExecutionTime)) {
            if (isItTimeForNextCall(nextCallTime)) {
                addNewClientToQueue(taskExecutor);
                nextCallTime = System.currentTimeMillis() + rnd.nextInt(MAX_TIME_BETWEEN_CALLS_SEC * 1000) + 1;
            }
            checkWaitingInQueueAndLeft();
        }

        while (!queue.isEmpty()) {
            checkWaitingInQueueAndLeft();
        }

        taskExecutor.shutdown();
    }

    private static int getNumberOfOperators() {
        int numberOfOperators = rnd.nextInt(MAX_OPERATORS_NUMBER) + 1;
        System.out.println("CallCenter start to work! " + numberOfOperators + " operator(s) are available!");
        return numberOfOperators;
    }

    private static boolean isCallCentreExecutionTimeOver(long startExecutionTime) {
        return System.currentTimeMillis() > startExecutionTime + CALL_CENTRE_TIME_EXECUTION_SEC * 1000;
    }

    private static boolean isItTimeForNextCall(long nextCallTime) {
        return System.currentTimeMillis() >= nextCallTime;
    }

    private static void addNewClientToQueue(ExecutorService taskExecutor) {
        SpeakTask currentSpeakTask = new SpeakTask(System.currentTimeMillis());
        try {
            queue.put(Map.entry(taskExecutor.submit(currentSpeakTask), currentSpeakTask));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Client №" + currentSpeakTask.clientNumber + " added to queue! " +
                "There are " + queue.size() + " client(s) in a queue now!");
    }

    private static void checkWaitingInQueueAndLeft() {
        for (Map.Entry<Future<?>, SpeakTask> task : queue) {
            SpeakTask currentSpeakTask = task.getValue();
            if (System.currentTimeMillis() >= (currentSpeakTask.startWaitingTime + currentSpeakTask.waitingTime)) {
                leftQueue(task, currentSpeakTask);
            }
        }
    }

    private static void leftQueue(Map.Entry<Future<?>, SpeakTask> task, SpeakTask currentSpeakTask) {
        int clientNumber = currentSpeakTask.clientNumber;
        task.getKey().cancel(false);
        if (queue.remove(task)) System.out.println("Client №" + clientNumber + " left a queue after " +
                currentSpeakTask.waitingTime / 1000d + " seconds waiting!");
    }

    private static class SpeakTask implements Runnable {
        public static final int OPERATOR_NUM_BEGIN_INDEX = 14;
        int clientNumber;
        public long startWaitingTime;
        public long waitingTime;

        public SpeakTask(long startWaitingTime) {
            this.startWaitingTime = startWaitingTime;
            waitingTime = rnd.nextInt(MAX_WAITING_FOR_CALL_TIME_SEC * 1000) + 1;
            this.clientNumber = CLIENT_NUMBER.incrementAndGet();
        }

        @Override
        public void run() {
            String operatorNumber = Thread.currentThread().getName().substring(OPERATOR_NUM_BEGIN_INDEX);
            takeCall(operatorNumber);
            int callSpeakingTime = getCallSpeakingTimeAndWait();
            endCall(operatorNumber, callSpeakingTime);
        }

        private void takeCall(String operatorNumber) {
            try {
                queue.take();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println("Operator " + operatorNumber + " take a call from client №" + clientNumber + "! " +
                    "Client " + clientNumber + " waiting for " +
                    (System.currentTimeMillis() - startWaitingTime) / 1000d + " seconds before operator's answer!");
        }

        private static int getCallSpeakingTimeAndWait() {
            int callSpeakingTime = rnd.nextInt(MAX_CALL_SPEAKING_TIME_SEC * 1000) + 1;
            try {
                Thread.sleep(callSpeakingTime);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return callSpeakingTime;
        }

        private void endCall(String operatorNumber, int callSpeakingTime) {
            System.out.println("Operator " + operatorNumber + " finish call with client №" + clientNumber + "! " +
                    "Call duration " + (callSpeakingTime / 1000d) + " seconds! " +
                    "There are " + queue.size() + " client(s) in a queue!");
        }
    }
}