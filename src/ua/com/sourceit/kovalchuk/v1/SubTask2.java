package ua.com.sourceit.kovalchuk.v1;

import java.io.*;
import java.util.*;

public class SubTask2 {
    public static final int QUANTITY_OF_NUMBERS = 3000;
    public static final String OUTPUT_FILE = "src/ua/com/sourceit/kovalchuk/v1/out2.txt";

    public static void main(String[] args) {
        createAndFillFile();
        sortFile();
    }

    private static void sortFile() {
        List<String> stringList = new ArrayList<>();
        readFromFile(stringList);
        Collections.sort(stringList);
        writeToFile(stringList);
    }

    private static void readFromFile(List<String> stringList) {
        try (BufferedReader br = new BufferedReader(new FileReader(OUTPUT_FILE))) {
            while (true) {
                String currentLine = br.readLine();
                if (currentLine == null) break;
                stringList.add(currentLine);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeToFile(List<String> stringList) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(OUTPUT_FILE))) {
            for (String currentLine : stringList) {
                bw.write(currentLine);
                bw.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createAndFillFile() {
        Random rnd = new Random();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(OUTPUT_FILE))) {
            for (int i = 0; i < QUANTITY_OF_NUMBERS; i++) {
                int randomNumber = rnd.nextInt();
                bw.write(String.valueOf(randomNumber));
                bw.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
